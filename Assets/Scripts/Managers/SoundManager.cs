using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour
{
    [Header("References")]
    public AudioSource audioSource;

    [Header("Settings")]
    public List<AudioSFX> sounds;

    private SoundEffectType _lastSoundEffect;

    public void Play(SoundEffectType sfxType, bool isRandom = false, float volume = 1f)
    {
        if (sounds.Count == 0)
            return;

        var sound = sounds.Find(i => i.soundEffectType == sfxType);

        if (sound == null)
            return;

        List<AudioClip> clips = sound.soundEffects;
        AudioClip clip = clips[0];

        if (volume == 1f)
            volume = sound.volume;

        if (isRandom)
            clip = clips[UnityEngine.Random.Range(0, clips.Count)];

        if (_lastSoundEffect == sfxType && !audioSource.isPlaying)
            audioSource?.PlayOneShot(clip, volume);
        else if (_lastSoundEffect != sfxType)
            audioSource?.PlayOneShot(clip, volume);

        _lastSoundEffect = sfxType;
    }

    public void PlayAgain(SoundEffectType sfxType)
    {
        if (sounds.Count == 0)
            return;

        var sound = sounds.Find(i => i.soundEffectType == sfxType);

        if (sound == null)
            return;

        List<AudioClip> clips = sound.soundEffects;
        AudioClip clip = clips[0];

        audioSource?.PlayOneShot(clip, sound.volume);
    }

    private void OnValidate()
    {
        if (audioSource != null)
            audioSource = GetComponent<AudioSource>();
    }

}

public enum SoundEffectType
{
    PLAYER_DEAD,
    PLAYER_PUNCH,
    PLAYER_JUMP_DOWN,
    PLAYER_WALK,
    UI_MENU,
    GAMEPLAY_POWER_UP,
    GAMEPLAY_LEVEL_UP,
    PROJECTILE_EXPLOSION,
    PROJECTILE_TRIGGER

}

[Serializable]
public class AudioSFX
{
    public SoundEffectType soundEffectType;
    public List<AudioClip> soundEffects;
    [Range(0, 1)] public float volume = 1f;
}
